﻿using TestTaskEpamDotNet.Framework;

namespace TestTaskEpamDotNet.Tests
{
    [TestFixture]
    internal class AutorizationTest : BaseTest
    {
        [Test]
        [Parallelizable]
        [TestCaseSource(typeof(TestData), nameof(TestData.InvalidCredentialsTestData))]
        public void LoginFormWithEmptyCredentialsTest(string username, string password)             
        {
            var expectedErrorMessage = "Username is required";

            Pages.Autorizations.Open();

            Pages.Autorizations.AddUsername(username);
            Pages.Autorizations.AddPassword(password);
            Pages.Autorizations.ClearUsername();
            Pages.Autorizations.ClearPassword();
            Pages.Autorizations.ClickLoginButton();
            
            var actualErrorMessage = Pages.Autorizations.GetResultClickLoginButton();
            Assert.That(expectedErrorMessage, Is.SubsetOf(actualErrorMessage));
        }

        [Test]
        [Parallelizable]
        [TestCaseSource(typeof(TestData), nameof(TestData.InvalidCredentialsTestData))]
        public void LoginFormWithCredentialsByPassingUsernameTest(string username, string password)
        { 
            var expectedErrorMessage = "Password is required";

            Pages.Autorizations.Open();

            Pages.Autorizations.AddUsername(username);
            Pages.Autorizations.AddPassword(password);
            Pages.Autorizations.ClearPassword();
            Pages.Autorizations.ClickLoginButton();

            var actualErrorMessage = Pages.Autorizations.GetResultClickLoginButton();
            Assert.That(expectedErrorMessage, Is.SubsetOf(actualErrorMessage));
        }

        [Test]
        [Parallelizable]
        [TestCaseSource(typeof(TestData), nameof(TestData.ValidCredentialsTestData))]
        public void LoginFormWithCredentialsByPassingUsernameAndPasswordTest(string username, string password)            
        {
            var expectedErrorMessage = "Swag Labs";

            Pages.Autorizations.Open();

            Pages.Autorizations.AddUsername(username);
            Pages.Autorizations.AddPassword(password);
            Pages.Autorizations.ClickLoginButton();

            var actualErrorMessage = Pages.Inventorys.GetLogo();
            Assert.That(actualErrorMessage, Is.EqualTo(expectedErrorMessage));
        }
        
    }
}
