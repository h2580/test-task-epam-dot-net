﻿using OpenQA.Selenium;

namespace TestTaskEpamDotNet.Pages
{
    internal class AutorizationPage : BasePage
    {
        public AutorizationPage(IWebDriver driver) : base(driver)
        {
        }

        protected override string Url => "https://www.saucedemo.com/";

        private IWebElement _usernameInput => _driver.FindElement(By.Id("user-name"));
   
        private IWebElement _passwordInput => _driver.FindElement(By.Id("password"));
    
        private IWebElement _loginInput => _driver.FindElement(By.Id("login-button"));
   
        private IWebElement _resultClickLoginButton => _driver.FindElement(By.XPath("//h3[@data-test='error']"));
         
        private IWebElement _logo => _driver.FindElement(By.XPath("//div[@class='app_logo']"));

            
        public void AddUsername(string username)
        {
            _usernameInput.SendKeys(username);
        }

        public void AddPassword(string password)
        {
            _passwordInput.SendKeys(password);
        }

        public void ClickLoginButton()
        {
            _passwordInput.SendKeys(Keys.Enter);
        }

        public void ClearUsername()
        {
            _usernameInput.SendKeys(Keys.Control + "A");
            _usernameInput.SendKeys(Keys.Backspace);
        }

        public void ClearPassword()
        {
            _passwordInput.SendKeys(Keys.Control + "A");
            _passwordInput.SendKeys(Keys.Backspace);
        }

        public string GetResultClickLoginButton()
        {
            return _resultClickLoginButton.Text;
        }
        
        public string GetLogo()
        {
            return _logo.Text;
        }
    }
}
