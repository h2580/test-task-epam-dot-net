﻿using OpenQA.Selenium;

namespace TestTaskEpamDotNet.Pages
{
    internal class InventoryPage : BasePage
    {
        public InventoryPage(IWebDriver driver) : base(driver)
        {
        }

        protected override string Url => "https://www.saucedemo.com/inventory.html";

        private IWebElement _logo => _driver.FindElement(By.XPath("//div[@class='app_logo']"));
        
        public string GetLogo()
        {
            return _logo.Text;
        }       
    }
}
