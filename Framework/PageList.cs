﻿using OpenQA.Selenium;
using TestTaskEpamDotNet.Pages;

namespace TestTaskEpamDotNet.Framework
{
    internal class PageList
    {
        public AutorizationPage Autorizations => _autorizations ??= new AutorizationPage(_driver);
        private AutorizationPage _autorizations;

        public AutorizationPage Inventorys => _inventorys ??= new AutorizationPage(_driver);
        private AutorizationPage _inventorys;

        private readonly IWebDriver _driver;

        public PageList(IWebDriver driver)
        {
            _driver = driver;
        }
    }
}
