﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTaskEpamDotNet.Framework
{
    internal class TestData
    {
        public static IEnumerable<TestCaseData> ValidCredentialsTestData()
        {
            yield return new TestCaseData("standard_user", "secret_sauce");
            yield return new TestCaseData("problem_user", "secret_sauce");
            yield return new TestCaseData("performance_glitch_user", "secret_sauce");
        }
        public static IEnumerable<TestCaseData> InvalidCredentialsTestData()
        {
            yield return new TestCaseData("locked_out_user", "secret_sauce");
            yield return new TestCaseData("alex", "12345");
            yield return new TestCaseData("mary", "secret_sauce");
            yield return new TestCaseData("standard_user", "45781");
        }
    }
}
