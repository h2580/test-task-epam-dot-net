﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;

namespace TestTaskEpamDotNet.Framework
{
    internal class DriverHelper
    {
        public static IWebDriver GetDriver()
        {
            var driver = new ChromeDriver();
            return driver;
        }

        internal static string MakeScreenshot(IWebDriver driver, string testName)
        {
            string screenshotsFolder = @"D:\Reports";
            string screenshotPath = string.Empty;

            if (driver != null)
            {
                Screenshot screenshot = ((ITakesScreenshot)driver).GetScreenshot();

                var dateTimeStr = DateTime.Now.ToString("yyyy-MM-dd-hhmm-ss");
                var screenshotName = $"{testName}-{dateTimeStr}.png";

                screenshotPath = Path.Combine(screenshotsFolder, screenshotName);
                screenshot.SaveAsFile(screenshotPath, ScreenshotImageFormat.Png);
            }

            return screenshotPath;
        }
    }
}
